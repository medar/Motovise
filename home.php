<?php get_header(); ?>

<main class="site-main site-main--blog clearfix">
	<div class="blog-search">
		<div class="container">
			<div class="blog-search__wrapper">
				<h1>Blog</h1>
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>

	<?php motovise_recent_posts( 9, true ); ?>

</main>

<?php get_footer(); ?>
