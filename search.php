<?php get_header(); ?>

<main class="site-main site-main--blog clearfix">
	<div class="blog-search">
		<div class="container">
			<div class="blog-search__wrapper">
				<h1>Blog</h1>
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
	<!-- <div class="blog-content <?php if( is_single() ): echo 'blog-content--relevant'; endif; ?>">
		<div class="container">
			<div class="blog-content__wrapper">

	<?php
	global $post;
	while (have_posts()) : the_post();
	?>

	<div class="blog-card">
	<a class="blog-card__img" href="<?php echo get_permalink(); ?>">
		<?php
	if ( has_post_thumbnail( ) ) :
		the_post_thumbnail( $post->ID, $size = 'post-preview' );
	endif;
	?>

	</a>
	<div class="blog-card__text">
		<a href="<?php get_permalink(); ?>"><h3>
				<?php the_title('', '', $echo = true); ?>
		</h3></a>
		<p><?php the_excerpt(); ?></p>
		</div>

		<div class="blog-card__date">
		<p>$time</p>
		</div>

		</div>
<?php
	endwhile;

	 ?>
 </div>
</div>
</div> -->
<?php motovise_search_result( ); ?>
</main>

<?php get_footer(); ?>
