<?php get_header();
if ( function_exists( 'ot_get_option' ) ) {
$book_inspection_url = ot_get_option( 'book_inspection_url', '#' );
$inspection_url = ot_get_option( 'inspection_url', '#' );
}
?>

<main class="site-main site-main--index clearfix">
	<div class="first-screen">
		<div class="first-screen__bg">
			<picture>
				<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg@2x.jpg 2x" media="(min-width: 992px)">
				<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg--768.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg--768.jpg 2x" media="(min-width: 480px)">
				<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg--480.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg--480.jpg 2x" media="(max-width: 479px)">
					<img src="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg@2x.jpg" alt="">
			</picture>
		</div>
		<div class="first-screen__content">
			<div class="container">
				<div class="first-screen__center">
					<h1><?php echo get_bloginfo( 'name' ); ?></h1>
					<div class="first-screen__buttons"><a class="btn btn--accent" href="/book-inspection"><span>Book now</span>
							<svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" viewbox="0 0 7 13">
								<path d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
							</svg></a><a class="btn btn--second" href="/how-it-works"><span>How it Works</span>
							<svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" viewbox="0 0 7 13">
								<path d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
							</svg></a></div>
					<div class="first-screen__bottom"><a class="scroll-down" href="#indexWhy">
							<div class="scroll-down__text">The why and how</div>
							<div class="scroll-down__arrow"><span></span><span></span></div></a></div>
				</div>
			</div>
		</div>
	</div>
	<div class="index-why" id="indexWhy">
		<div class="container">
			<div class="index-why__first why-first">
				<div class="why-first__heading">
					<h2>WHY USE MOTOVISE?</h2>
				</div>
				<div class="why-first__text">
					<p><?php echo get_field( 'why_use_motovise' ); ?></p>
				</div>
			</div>
			<div class="index-why__second why-second">
				<div class="why-second__benefits">
					<div class="benefit-card">
						<div class="benefit-card__img">
							<svg xmlns="http://www.w3.org/2000/svg" width="57.51" height="60" viewbox="0 0 57.51 60">
								<path fill="#e82828" d="M42 16.75a16.91 16.91 0 0 0-3-.63V3H3v54h36v-7.12a16.91 16.91 0 0 0 3-.63V60H0V0h42v16.75zm-7-.63A16.9 16.9 0 0 0 27.35 19H7v-3h28v.12zM22.58 24a16.94 16.94 0 0 0-1.49 3H7v-3h15.58zM20 32v1a17.18 17.18 0 0 0 .12 2H7v-3h13zm1.47 8a17 17 0 0 0 1.75 3H7v-3h14.5zM29 48a16.89 16.89 0 0 0 6 1.88V51H7v-3h22zM7 8h28v3H7V8z"></path>
								<path fill="#e82828" d="M46.17 43.58a14 14 0 1 1 2-2.2l9.29 9.29-2.08 2.13-9.22-9.22zm-1.39-2.8a11 11 0 1 0-15.56 0 11 11 0 0 0 15.56 0z"></path>
							</svg>
						</div>
						<div class="benefit-card__text">
							<h3>innovative reports</h3>
							<p><?php echo get_field( 'innovative_reports' ); ?></p>
						</div>
					</div>
					<div class="benefit-card">
						<div class="benefit-card__img">
							<svg xmlns="http://www.w3.org/2000/svg" width="71" height="60" viewbox="0 0 71 60">
								<path fill="#e82828" d="M34 0h3v5.09h-3V0zm6 9.18c7.69-2.74 15.33-5.6 23-8.37v8.25a4.26 4.26 0 0 0 .83 2.12c1.57 2.6 3 5.28 4.6 7.86L71 19v.84a9.65 9.65 0 0 1-5.1 8.06A9.52 9.52 0 0 1 54.56 26 10.19 10.19 0 0 1 52 19l2.36-.1c1.74-3.26 3.7-6.37 5.47-9.61.41-1.29.08-2.79.17-4.18-6.31 2.3-12.63 4.58-18.94 6.89a5.62 5.62 0 0 1-10.13 3.7c-6.65 2.38-13.28 4.82-19.93 7.24a66.1 66.1 0 0 0 .13 6.9c1.68 3.1 3.49 6.14 5.29 9.18L19 39a10.17 10.17 0 0 1-2.86 7.22A9.52 9.52 0 0 1 5.13 47.9 9.64 9.64 0 0 1 0 39.83V39c.81-.17 2.06.39 2.52-.53 1.79-3.16 3.73-6.25 5.41-9.47.18-2.73 0-5.48.09-8.22 7.33-2.51 14.58-5.29 21.89-7.89A5.64 5.64 0 0 1 40 9.18m-5.38 1a2.41 2.41 0 0 0-1 3.71 2.38 2.38 0 0 0 4.1-.22c1-1.81-1.06-4.41-3.06-3.49M57.69 19h7.21c-1.2-2.07-2.4-4.15-3.59-6.23-1.23 2.06-2.42 4.16-3.62 6.24m-2.22 3a6.85 6.85 0 0 0 3.92 3.61A6.63 6.63 0 0 0 67.53 22H55.47M5.7 39h7.21c-1.19-2.07-2.4-4.13-3.57-6.21-1.28 2-2.42 4.15-3.63 6.22M3.47 42a6.9 6.9 0 0 0 3.94 3.61A6.63 6.63 0 0 0 15.53 42H3.47z"></path>
								<path fill="#e82828" d="M34 19.88a16.7 16.7 0 0 0 3 0V42h-3V19.88zM29 44h13v3H29v-3zm-2.82 6.15c2.88-1.5 6.2-1.1 9.32-1.15s6.45-.35 9.33 1.15A11.22 11.22 0 0 1 51 60H20a11.23 11.23 0 0 1 6.2-9.85M23.56 57h23.89A8.22 8.22 0 0 0 40 52c-3.19 0-6.4-.07-9.59 0a8.17 8.17 0 0 0-6.85 5z"></path>
							</svg>
						</div>
						<div class="benefit-card__text">
							<h3>unbiased</h3>
							<p><?php echo get_field( 'unbiased' ); ?></p>
						</div>
					</div>
					<div class="benefit-card">
						<div class="benefit-card__img">
							<svg xmlns="http://www.w3.org/2000/svg" width="55" height="56" viewbox="0 0 55 56">
								<path fill="#e82828" d="M23.38 0h3.72a8 8 0 0 1 7.83 7.43c.15 4.18 0 8.38.06 12.57 4.19 0 8.38-.09 12.56.06A8 8 0 0 1 55 27.39v1.2q-1.67 10.79-3.42 21.58A7.14 7.14 0 0 1 45.35 56H18.81a7.85 7.85 0 0 1-4-1.61A7.75 7.75 0 0 1 11.56 56h-7A5.22 5.22 0 0 1 0 51.56V25.71A5.11 5.11 0 0 1 5.47 21c2.94.15 6.23-.65 8.83 1.14 2.14-2.07 5.43-2.66 7-5.34 3.14-5 2.6-11.21 2.08-16.81m3.31 2.93c.19 4.42.15 9-1.59 13.16a12.26 12.26 0 0 1-6.38 6.66C17.38 23.3 16 24.36 16 26v23.55A3.1 3.1 0 0 0 19 53h25.58a4.09 4.09 0 0 0 4.08-3.52c1-6.34 2-12.7 3-19.05a8.58 8.58 0 0 0 .09-3.87A5 5 0 0 0 47 23c-5-.05-10 0-15 0 0-5.19.09-10.38-.06-15.57a5.19 5.19 0 0 0-5.26-4.5M4.17 24.19A2.22 2.22 0 0 0 3 26.47v24.08A2.07 2.07 0 0 0 5 53q3 .1 6.08 0A2.08 2.08 0 0 0 13 50.54V26a2.06 2.06 0 0 0-2.4-2 38.48 38.48 0 0 0-6.43.19z"></path>
								<path fill="#e82828" d="M7.18 46.19c2.31-1.18 4 2.71 1.61 3.63-2.3 1.18-3.95-2.7-1.61-3.63z"></path>
							</svg>
						</div>
						<div class="benefit-card__text">
							<h3>convenient</h3>
							<p><?php echo get_field( 'convenient' ); ?></p>
						</div>
					</div>
					<div class="benefit-card">
						<div class="benefit-card__img">
							<svg xmlns="http://www.w3.org/2000/svg" width="68" height="68" viewbox="0 0 68 68">
								<path fill="#e82828" d="M25.93 0h1.68a26.82 26.82 0 0 1 23.12 15.78C55.83 25.94 61.15 36 66.31 46.11A16 16 0 0 1 68 52.44v1.31A15.26 15.26 0 0 1 53.92 68h-1.24a15.5 15.5 0 0 1-6.1-1.47C35.65 61 24.76 55.3 13.82 49.76A26.87 26.87 0 0 1 0 27.83v-1.75A26.57 26.57 0 0 1 25.93 0m-3.68 3.4A23.47 23.47 0 0 0 16 47.47Q32 55.7 48 63.9c5.88 2.9 13.64-.22 16-6.29A11.92 11.92 0 0 0 63.41 47c-5.5-10.81-11.15-21.53-16.62-32.36A23.56 23.56 0 0 0 22.25 3.4z"></path>
								<path fill="#e82828" d="M23.1 8h7.78l1.86 5.19c1.63-.73 3.24-1.53 4.9-2.21 1.77 1.81 3.56 3.61 5.38 5.36-.66 1.67-1.49 3.27-2.21 4.91L46 23.11v7.78l-5.19 1.86c.74 1.6 1.47 3.22 2.23 4.81q-2.69 2.79-5.48 5.44c-1.6-.76-3.21-1.49-4.82-2.23L30.89 46h-7.78l-1.86-5.19L16.41 43q-2.74-2.68-5.41-5.41c.75-1.62 1.49-3.22 2.23-4.85L8 30.89v-7.78l5.19-1.86q-1.11-2.42-2.19-4.83 2.72-2.75 5.46-5.46l4.84 2.23L23.1 8m2.11 3l-1.78 5-1.77.69L17 14.56 14.56 17c.71 1.53 1.42 3.08 2.13 4.62L16 23.43l-5 1.78v3.56c1.66.59 3.33 1.18 5 1.78l.69 1.77c-.69 1.56-1.42 3.1-2.13 4.68L17 39.44c1.53-.7 3.05-1.42 4.59-2.1.6.28 1.21.53 1.83.78.61 1.62 1.16 3.25 1.75 4.88h3.56c.59-1.66 1.18-3.33 1.78-5l1.77-.69L37 39.44 39.44 37c-.7-1.53-1.42-3-2.1-4.59.28-.61.53-1.21.78-1.82 1.62-.62 3.25-1.17 4.88-1.76v-3.56l-5-1.78-.69-1.77c.69-1.59 1.43-3.14 2.13-4.72L37 14.56l-4.62 2.13-1.81-.69c-.61-1.66-1.19-3.32-1.78-5h-3.57z"></path>
								<path fill="#e82828" d="M25.18 19.25c4.4-1.19 9.14 2.27 9.72 6.7a8.15 8.15 0 0 1-7.37 9 8.17 8.17 0 0 1-8.53-7.4 8.18 8.18 0 0 1 6.14-8.3m.06 3.13A5.22 5.22 0 0 0 22 27a5.18 5.18 0 0 0 5 5 5.1 5.1 0 0 0 4.86-6c-.62-2.81-3.86-4.85-6.62-3.62zm24.54 21.45a8.48 8.48 0 1 1-6.08 9.71 8.52 8.52 0 0 1 6.08-9.71m.47 3c-3.9 1.15-5 6.8-1.81 9.33a5.53 5.53 0 0 0 8.9-2.72c1.3-4.04-3.16-8.2-7.08-6.63z"></path>
							</svg>
						</div>
						<div class="benefit-card__text">
							<h3>technical</h3>
							<p><?php echo get_field( 'technical' ); ?></p>
						</div>
					</div>
				</div>
				<div class="why-second__book">
					<div class="index-book__wrapper">
						<div class="index-book__img">
							<picture>
								<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-book__img.png 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/index-book__img@2x.png 2x" media="(min-width: 768px)">
								<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-book__img--767.png 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/index-book__img@2x--767.png 2x" media="(max-width: 767px)">
									<img src="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-book__img.png" alt="">
							</picture>
						</div><a class="index-book__btn accent-btn" href="<?php echo $book_inspection_url ?>">Book Inspection</a><a class="index-book__btn secondary-btn" href="<?php echo $inspection_url ?>">What's included →</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="index-how">
		<div class="container">
			<h2>how it works</h2>
			<div class="index-how__wrapper">
				<div class="how-card">
					<div class="how-card__img">
						<svg xmlns="http://www.w3.org/2000/svg" width="77" height="76" viewbox="0 0 77 76">
							<path fill="#e82828" d="M58 54h9v3H55V42h3v12zm0-22h-3V20H3v40h30.43a23.33 23.33 0 0 0 .79 3H0V5h10V0h10v5h18V0h10v5h10v27zm-19 7.82A23.57 23.57 0 0 0 36 44h-5v-8h8v3.82zM34.22 48A23.47 23.47 0 0 0 33 55.5v.5h-2v-8h3.22zm9.16-12l-.38.26V36h.38zM38 6.5V8H20v6H10V8H3v9h52V8h-7v6H38V6.5zM7 24h8v8H7v-8zm3 3v2h2v-2h-2zm-3 9h8v8H7v-8zm3 3v2h2v-2h-2zm-3 9h8v8H7v-8zm3 3v2h2v-2h-2zm9-27h8v8h-8v-8zm3 3v2h2v-2h-2zm-3 9h8v8h-8v-8zm3 3v2h2v-2h-2zm-3 9h8v8h-8v-8zm3 3v2h2v-2h-2zm9-27h8v8h-8v-8zm3 3v2h2v-2h-2zm0 12v2h2v-2h-2zm9-15h8v8h-8v-8zm3 3v2h2v-2h-2zM13 3v8h4V3h-4zm28 0v8h4V3h-4zm15.5 73A20.5 20.5 0 1 1 77 55.5 20.5 20.5 0 0 1 56.5 76zm0-3A17.5 17.5 0 1 0 39 55.5 17.5 17.5 0 0 0 56.5 73z"></path>
						</svg>
					</div>
					<div class="how-card__text">
						<h3>1. book inspection</h3>
						<p><?php echo get_field( 'book_inspection' ); ?></p><a href="<?php echo get_field( 'book_inspection_link' ); ?>">Book it now →</a>
					</div>
				</div>
				<div class="how-card">
					<div class="how-card__img">
						<svg xmlns="http://www.w3.org/2000/svg" width="82" height="89" viewbox="0 0 82 89">
							<path fill="none" stroke="#e82828" stroke-width="3px" d="M1.5 49.5h79v7h-79z"></path>
							<path fill="#e82828" d="M10 86h62v3H10z"></path>
							<path fill="#e82828" d="M22.002 65.003l17.32-10 1.5 2.598-17.32 10z"></path>
							<path fill="#e82828" d="M40.996 75.004l17.32-10 1.5 2.598-17.32 10z"></path>
							<path fill="#e82828" d="M22.002 86l17.32-10 1.5 2.598-17.32 10z"></path>
							<path fill="#e82828" d="M41.002 57.603l1.5-2.598 17.32 10-1.5 2.598z"></path>
							<path fill="#e82828" d="M22.002 67.597l1.5-2.598 17.32 10-1.5 2.596z"></path>
							<path fill="#e82828" d="M41 78.603l1.5-2.598 17.32 10-1.5 2.598z"></path>
							<circle fill="#e82828" cx="41" cy="77" r="4"></circle>
							<circle fill="#e82828" cx="22" cy="66" r="4"></circle>
							<circle fill="#e82828" cx="60" cy="66" r="4"></circle>
							<path fill="#e82828" d="M9.67 42H4a4 4 0 0 1-4-4V26a9 9 0 0 1 9-9h73v15a10 10 0 0 1-10 10h-.67a7 7 0 0 0 .67-3v-.06A8 8 0 0 0 79 31V20H10a7 7 0 0 0-7 7v10a2 2 0 0 0 2 2h4a7 7 0 0 0 .67 3zm12.65 0a7 7 0 0 0 .68-3h35a7 7 0 0 0 .67 3H22.33z"></path>
							<path fill="#e82828" d="M36 0h37a9 9 0 0 1 9 9v11H17v-1A19 19 0 0 1 36 0zm0 3a16 16 0 0 0-16 16v1h59V9a6 6 0 0 0-6-6H36z"></path>
							<path fill="#e82828" d="M40 1h3v19h-3z"></path>
							<path fill="#e82828" d="M56.004 2.03l2.82-1.027 6.497 17.854-2.817 1.027z"></path>
							<path fill="#e82828" d="M16 48a9 9 0 1 1 9-9 9 9 0 0 1-9 9zm0-3a6 6 0 1 0-6-6 6 6 0 0 0 6 6z"></path>
							<path fill="#e82828" d="M65 48a9 9 0 1 1 9-9 9 9 0 0 1-9 9zm0-3a6 6 0 1 0-6-6 6 6 0 0 0 6 6z"></path>
							<path fill="#e82828" d="M2.5 25h6a1.5 1.5 0 0 1 1.5 1.5A1.5 1.5 0 0 1 8.5 28h-6A1.5 1.5 0 0 1 1 26.5 1.5 1.5 0 0 1 2.5 25z"></path>
							<path fill="#e82828" d="M75.5 22h5a1.5 1.5 0 0 1 1.5 1.5 1.5 1.5 0 0 1-1.5 1.5h-5a1.5 1.5 0 0 1-1.5-1.5 1.5 1.5 0 0 1 1.5-1.5z"></path>
						</svg>
					</div>
					<div class="how-card__text">
						<h3>2. THE NITTY-GRITTY</h3>
						<p><?php echo get_field( 'the_nitty-gritty' ); ?></p><a href="<?php echo get_field( 'the_nitty_gritty_url' ); ?>">What's Included →</a> 
					</div>
				</div>
				<div class="how-card">
					<div class="how-card__img">
						<svg xmlns="http://www.w3.org/2000/svg" width="57.51" height="60" viewbox="0 0 57.51 60">
							<path fill="#e82828" d="M42 16.75a16.91 16.91 0 0 0-3-.63V3H3v54h36v-7.12a16.91 16.91 0 0 0 3-.63V60H0V0h42v16.75zm-7-.63A16.9 16.9 0 0 0 27.35 19H7v-3h28v.12zM22.58 24a16.94 16.94 0 0 0-1.49 3H7v-3h15.58zM20 32v1a17.18 17.18 0 0 0 .12 2H7v-3h13zm1.47 8a17 17 0 0 0 1.75 3H7v-3h14.5zM29 48a16.89 16.89 0 0 0 6 1.88V51H7v-3h22zM7 8h28v3H7V8z"></path>
							<path fill="#e82828" d="M46.17 43.58a14 14 0 1 1 2-2.2l9.29 9.29-2.08 2.13-9.22-9.22zm-1.39-2.8a11 11 0 1 0-15.56 0 11 11 0 0 0 15.56 0z"></path>
						</svg>
					</div>
					<div class="how-card__text">
						<h3>3. get report</h3>
						<p><?php echo get_field( 'get_report' ); ?></p><a href="<?php echo get_field( 'get_report_url' ); ?>">SEE SAMPLE →</a> 
					</div>
				</div>
			</div><a class="index-how__btn" href="/how-it-works">Learn more</a>
		</div>
	</div>
	<div class="index-testimonials">
		<div class="container">
			<h2>What our customers say</h2>
			<div class="index-testimonials__wrapper">
				<div class="index-testimonials__slider">
					<?php
					global $post;
					$args = array( 'posts_per_page' => 10, 'offset' => 0, 'post_type' => 'motovise_testimonial' );

					$myposts = get_posts( $args );
									foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
										<div class="index-testimonials__item">
											<p><?php the_content(); ?></p>
											<cite><a><?php echo get_field( 'name' ); ?></a></cite>
										</div>
									<?php endforeach;
									wp_reset_postdata();
					 ?>

				</div>
				<div class="index-testimonials__slider-controls"><span class="index-testimonials__slider--left">
                <svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" viewbox="0 0 7 13">
                  <path d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
                </svg></span><span class="index-testimonials__slider--right">
                <svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" viewbox="0 0 7 13">
                  <path d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
                </svg></span></div>
			</div>
		</div>
	</div>
	<div class="index-price">
		<div class="container">
			<h2>Inspection Price</h2>
			<div class="price-card">
				<div class="price-card__img">
					<picture>
							<?php echo wp_get_attachment_image(get_field('inspection_price_img'), $size=array(600,600)); ?>
					</picture>
				</div>
				<div class="price-card__data"><span>$<?php
				 if ( function_exists( 'ot_get_option' ) ) {
				  echo ot_get_option( 'inspection_price', '0' );
				 }
				?></span>
					<h3><?php echo get_field( 'inspection_price_title' ); ?></h3>
					<p><?php echo get_field( 'inspection_price_content' ); ?></p><a class="index-price__btn accent-btn" href="<?php echo $inspection_url; ?>">Learn More</a>
				</div>
			</div>
		</div>
	</div>
	<div class="index-where">
		<div class="container">
			<h2>where we operate</h2>
			<div class="index-where__wrapper">
				<div class="where-item">
					<div class="where-item__img">
						<picture>
							<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/ny.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/ny@2x.jpg 2x" media="(min-width: 768px)">
							<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/ny--767.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/ny@2x--767.jpg 2x" media="(max-width: 767px)">
								<img src="<?php echo get_template_directory_uri() . '/'; ?>img/index/ny.jpg" alt="">
						</picture>
					</div>
					<div class="where-item__text"><span>ny</span>
						<p>New York</p>
					</div>
				</div>
				<div class="where-item">
					<div class="where-item__img">
						<picture>
							<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/nj.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/nj@2x.jpg 2x" media="(min-width: 768px)">
							<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/nj--767.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/nj@2x--767.jpg 2x" media="(max-width: 767px)">
								<img src="<?php echo get_template_directory_uri() . '/'; ?>img/index/nj.jpg" alt="">
						</picture>
					</div>
					<div class="where-item__text"><span>nj</span>
						<p>new Jersey</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
