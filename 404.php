<?php get_header();
?>

<main class="site-main site-main--index clearfix">
	<div class="first-screen">
		<div class="first-screen__bg">
			<picture>
				<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg@2x.jpg 2x" media="(min-width: 992px)">
				<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg--768.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg--768.jpg 2x" media="(min-width: 480px)">
				<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg--480.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg--480.jpg 2x" media="(max-width: 479px)">
					<img src="<?php echo get_template_directory_uri() . '/'; ?>img/index/index-bg@2x.jpg" alt="">
			</picture>
		</div>
		<div class="first-screen__content">
			<div class="container">
				<div class="first-screen__center">
					<h1>404<br>Page not found</h1>
					<div class="first-screen__buttons"><a class="btn btn--accent" href="/book-inspection"><span>Book now</span>
							<svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" viewbox="0 0 7 13">
								<path d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
							</svg></a><a class="btn btn--second" href="/how-it-works"><span>How it Works</span>
							<svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" viewbox="0 0 7 13">
								<path d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
							</svg></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
