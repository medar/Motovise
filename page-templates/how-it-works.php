<?php
/**
 * Template Name: How It Works
 *
 * Description: Template for How It Works page
 */
get_header();

 if ( function_exists( 'ot_get_option' ) ) {
  $book_inspection_url = ot_get_option( 'book_inspection_url', '#' );
 }
?>
<main class="site-main site-main--how-it-works clearfix">
  <div class="hiw-steps">
    <div class="container">
      <h1>HOW IT WORKS</h1>
      <div class="hiw-steps__wrapper">
        <div class="hiw-step">
          <div class="hiw-step__number"><span>1</span></div>
          <div class="hiw-step__data">
            <h2>Book inspection</h2>
            <p><?php echo get_field( 'book_inspection' ); ?></p>
          </div>
          <div class="hiw-step__img">
            <picture>

                <?php echo wp_get_attachment_image(get_field('book_inspection_img'), $size=array(600,600)); ?>

            </picture><a class="hiw-step__btn accent-btn" href="<?php echo $book_inspection_url; ?>">Book Inspection</a>
          </div>
        </div>
        <div class="hiw-step">
          <div class="hiw-step__number"><span>2</span></div>
          <div class="hiw-step__data">
            <h2>The Nitty-Gritty</h2>
            <p><?php echo get_field( 'the_nitty_gritty' ); ?></p>
          </div>
          <div class="hiw-step__img">
            <picture>
              <?php echo wp_get_attachment_image(get_field('the_nitty_gritty_img'), $size=array(600,600)); ?>
            </picture>
          </div>
        </div>
        <div class="hiw-step">
          <div class="hiw-step__number"><span>3</span></div>
          <div class="hiw-step__data">
            <h2>The report</h2>
            <p><?php echo get_field( 'the_report' ); ?></p>
          </div>
          <div class="hiw-step__img">
            <picture>
              <?php echo wp_get_attachment_image(get_field('the_report_img'), $size=array(600,600)); ?>
            </picture>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="hiw-learn-more poly-block">
    <div class="container">
      <h1>Want to learn more?</h1>
      <div class="hiw-learn-more__wrapper poly-block__wrapper"><a class="video-modal poly-block__btn" href="#videoModal">
          <svg xmlns="http://www.w3.org/2000/svg" width="19" height="26" viewbox="0 0 19 26">
            <path fill="#E82828" fill-rule="evenodd" d="M0 0v26l18.998-13"></path>
          </svg><span>Watch video</span></a></div>
    </div>
  </div>
  <div class="remodal video--modal" data-remodal-id="videoModal">
    <button class="remodal-close" data-remodal-action="close">
      <svg xmlns="http://www.w3.org/2000/svg" width="31.11" height="31.11" viewbox="0 0 31.11 31.11">
        <path fill="#e82828" d="M18.38 15.55L31.11 2.83 28.27 0 15.55 12.73 2.83 0 0 2.83l12.73 12.72L0 28.27l2.83 2.83 12.72-12.72 12.72 12.73 2.84-2.84z"></path>
      </svg>
    </button>
    <div class="remodal__body">
      <div class="video-container">
        <iframe width="560" height="315" src="<?php echo get_field( 'want_to_learn_more_youtube' ); ?>" frameborder="0" allowfullscreen=""></iframe>
      </div>
    </div>
  </div>
  <div class="hiw-faq">
    <div class="container">
      <h2>FAQs</h2>
      <div class="hiw-faq__wrapper">
        <div class="panel-group standard-wrapper" id="faq">
          <?php
          global $post;
          $args = array( 'posts_per_page' => 30, 'offset' => 0, 'post_type' => 'motovise_faq', 'orderby' => 'ID', 'order' => 'ASC' );
          $iterator = 1;
          $myposts = get_posts( $args );
                  foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                  <div class="panel">
                    <div id="heading--<?php echo $iterator; ?>">
                      <h4 class="panel-title"><a data-toggle="collapse" data-parent="#faq" href="#collapse--<?php echo $iterator; ?>"><?php echo the_title( $before = '', $after = '', $echo = true ); ?></a></h4>
                    </div>
                    <div class="panel-collapse collapse <?php if($iterator === 1): echo 'in'; endif; ?>" id="collapse--<?php echo $iterator; ?>">
                      <div class="panel-body">
                        <p><?php the_content(); ?></p>
                        <a class="accent-btn" href="<?php echo $book_inspection_url; ?>">Book Inspection</a>
                      </div>
                    </div>
                  </div>

                  <?php
                  $iterator++;
                 endforeach;
                  wp_reset_postdata();
           ?>

        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>
