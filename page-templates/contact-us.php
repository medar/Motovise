<?php
/**
 * Template Name: Contact Us
 *
 * Description: Template for Contact Us page
 */
get_header(); 

if ( function_exists( 'ot_get_option' ) ) {
$site_email = ot_get_option( 'site_email', '#' );
$site_phone = ot_get_option( 'site_phone', '#' );
}
?>

<main class="site-main site-main--contact-us clearfix">
  <div class="contacts-details">
    <div class="contacts-item">
      <div class="contacts-item__img">
        <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewbox="0 0 100 100">
          <circle cx="50" cy="50" r="50" fill="#f4f4f4"></circle>
          <path fill="#e82828" d="M33.11 35L50 51.82 66.89 35H33.11zM69 37.08L50 56 31 37.08V65h38V37.08zM28 32h44v36H28V32z"></path>
        </svg>
      </div>
      <div class="contacts-item__data">
        <p>Email us at:</p><a href="mailto:<?php echo $site_email ?>"><?php echo $site_email ?></a>
      </div>
    </div>
    <div class="contacts-item">
      <div class="contacts-item__img">
        <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewbox="0 0 100 100">
          <circle cx="50" cy="50" r="50" fill="#f4f4f4"></circle>
          <path fill="#e82828" d="M64 37H36v25h28V37zm0-3v-7H36v7h28zm0 31H36v8h28v-8zM33 24h34v52H33V24zm13 5h8v3h-8v-3zm4 42a2 2 0 1 1 2-2 2 2 0 0 1-2 2z"></path>
        </svg>
      </div>
      <div class="contacts-item__data">
        <p>Call us at:</p><a href="tel:<?php echo preg_replace('/\D/', '', $site_phone) ?>"><?php echo $site_phone ?></a>
      </div>
    </div>
  </div>
  <div class="contacts-form"> 
	<?php echo do_shortcode('[contact-form-7 id="130" title="Contact us"]'); ?> 
  </div>
</main>

<?php get_footer(); ?>
