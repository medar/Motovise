<?php
/**
 * Template Name: Book Inspecton
 *
 * Description: Template for Book Inspecton page
 */
get_header(); ?>

<main class="site-main site-main--book-inspection clearfix">
  <div class="book-inspection-content"><?php echo get_field('iframe_form'); ?></div>
</main>

<?php get_footer(); ?>
