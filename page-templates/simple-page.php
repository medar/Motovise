<?php
/**
 * Template Name: Simple page
 *
 * Description: Template for Book Inspecton page
 */
get_header(); ?>

<main class="site-main clearfix">
  <div class="article-content">
	<?php while ( have_posts() ) : the_post(); ?>
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>
	<?php endwhile; ?>
  </div>
</main>

<?php get_footer(); ?>
