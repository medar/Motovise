<?php
/**
 * Template Name: Inspections
 *
 * Description: Template for Inspections page
 */
get_header(); 
if ( function_exists( 'ot_get_option' ) ) {
$book_inspection_url = ot_get_option( 'book_inspection_url', '#' );
$sample_url = ot_get_option( 'sample_url', '#' );
}
?>

<main class="site-main site-main--inspections clearfix">
  <div class="inspections-intro">
  <?php   
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		//
		?>
			<h1><?php the_title() ?></h1>
			<div class="inspections-intro__wrapper">
				<p><?php the_content(); ?></p>
			</div>  
		<?php
		//
	} // end while
} // end if
  

   ?>
  </div>
  <div class="inspections-table">
    <div class="inspections-table__body">
      <div class="inspections-table__title"><span class="inspections-table__left"> What's included</span><span class="inspections-table__right"> Standard inspection<strong>$
		<?php
		 if ( function_exists( 'ot_get_option' ) ) {
		  echo ot_get_option( 'inspection_price', '0' );
		 }
		?>
	  </strong></span></div>
      <div class="inspections-table__data">
	  
	        <?php
			    global $post;
			    $args = array( 'posts_per_page' => 99999, 'offset' => 0, 'post_type' => 'motovise_inspections', 'orderby' => 'ID', 'order' => 'ASC' );
			    $iterator = 1;
			    $myposts = get_posts( $args );
                  foreach ( $myposts as $post ) : setup_postdata( $post ); 
					$inspection_price = get_field( 'standard_inspection' );
					$stInp = ' is-included';
				    if($inspection_price != true){ $stInp = ' is-not-included'; }
				  ?>
					<div class="inspections-table__row"><span class="inspections-table__left"> <?php echo the_title(  ); ?></span><span class="inspections-table__right<?php echo $stInp; ?>"><b>Standard inspection<strong>$ 
					<?php
					 if ( function_exists( 'ot_get_option' ) ) {
					  echo ot_get_option( 'inspection_price', '0' );
					 }
					 ?>
					</strong></b></span></div> 
                  <?php
                  $iterator++;
                endforeach;
                wp_reset_postdata();
            ?> 
      </div>
    </div>
	<div class="inspections-table__btn-wrapper"><a class="inspections-table__btn second-accent-btn" href="<?php echo $sample_url; ?>">See Sample</a><a class="inspections-table__btn accent-btn" href="<?php echo $book_inspection_url; ?>">Book Inspection</a></div>
  </div>
</main>

<?php get_footer(); ?>
