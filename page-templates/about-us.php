<?php
/**
 * Template Name: About Us
 *
 * Description: Template for About Us page
 */
get_header(); ?>

<main class="site-main site-main--about-us clearfix">
	<div class="about-content">
		<h1>ABOUT US</h1>
		<div class="about-content__item">
			<div class="about-content__img">
				<picture>
					<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/about/item1.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/about/item1@2x.jpg 2x"
							media="(min-width: 992px)">
					<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/about/item1--991.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/about/item1@2x--991.jpg 2x"
							media="(min-width: 480px)">
					<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/about/item1--479.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/about/item1@2x--479.jpg 2x"
							media="(max-width: 479px)">
					<img src="<?php echo get_template_directory_uri() . '/'; ?>img/about/item1.jpg" alt="">
				</picture>
				<div class="container">
					<div class="about-content__text">
						<p><?php echo get_field( 'text_1' ); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="about-content__item">
			<div class="about-content__img">
				<picture>
					<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/about/item2.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/about/item2@2x.jpg 2x"
							media="(min-width: 992px)">
					<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/about/item2--991.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/about/item2@2x--991.jpg 2x"
							media="(min-width: 480px)">
					<source srcset="<?php echo get_template_directory_uri() . '/'; ?>img/about/item2--479.jpg 1x, <?php echo get_template_directory_uri() . '/'; ?>img/about/item2@2x--479.jpg 2x"
							media="(max-width: 479px)">
					<img src="<?php echo get_template_directory_uri() . '/'; ?>img/about/item2.jpg" alt="">
				</picture>
				<div class="container">
					<div class="about-content__text">
						<p><?php echo get_field( 'text_2' ); ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="about-meet">
		<div class="container">
			<h2>Meet The Team</h2>
		</div>
		<?php
		global $post;
		$args = array( 'posts_per_page' => - 1, 'offset' => 0, 'post_type' => 'motovise_team' );
		$team = get_posts( $args );
		$i    = 1;
		foreach ( $team as $person ) :
			setup_postdata( $person );
			?>
			<div class="about-meet__show-more about-meet__show-more--hide" id="moreItem<?php echo $i; ?>">
				<div class="container">
					<div class="show-more-item">
						<div class="meet-item__img">
							<?php echo wp_get_attachment_image(
								get_field( 'photo_person', $person->ID ),
								$size = array( 180, 180 )
							); ?>
						</div>
						<div class="show-more-item__data">
							<h3 class="meet-item__heading"><?php echo get_the_title($person); ?></h3>
							<p class="meet-item__position"><?php echo get_field( 'position', $person->ID ); ?></p>
							<p class="show-more-item__text"><?php the_content(); ?></p>
							<button class="meet-item__btn meet-item__btn-hide" id="hideMoreBtn<?php echo $i; ?>">Hide</button>
						</div>
					</div>
				</div>
			</div>

			<?php
			$i += 1;
		endforeach;
		?>
		<div class="container">
			<div class="about-meet__wrapper">
				<?php
				$args = array( 'posts_per_page' => - 1, 'offset' => 0, 'post_type' => 'motovise_team' );
				$team = get_posts( $args );
				$i    = 1;
				foreach ( $team as $person ) :
					?>
					<div class="meet-item">
						<div class="meet-item__img">
							<?php echo wp_get_attachment_image(
								get_field( 'photo_person', $person->ID ),
								$size = array( 180, 180 )
							); ?>
						</div>
						<h3 class="meet-item__heading"><?php echo get_the_title($person); ?></h3>
						<p class="meet-item__position"><?php echo get_field( 'position', $person->ID ); ?></p>
						<button class="meet-item__btn meet-item__btn-show" id="showMoreBtn<?php echo $i; ?>">Show more</button>
					</div>
					<?php
					$i += 1;
				endforeach;
				?>
			</div>
		</div>
	</div>


	<div class="about-questions poly-block">
		<div class="container">
			<h2>Want to learn more?</h2>
			<div class="about-questions__wrapper poly-block__wrapper"><a class="about-questions__btn poly-block__btn"
																		 href="/contact-us/"><span>Contact Us</span></a>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
