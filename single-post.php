<?php get_header(); ?>

<main class="site-main site-main--article clearfix">
  <div class="article-img">
    <div class="article-img__wrapper">

      <?php
      if (has_post_thumbnail()) {
        echo '<picture>';
        echo get_the_post_thumbnail( $post->ID, 'post-header-full', array() );
        echo '</picture>';
      } else {
        ?>
        <picture class="empty-article-picture">
        </picture>

        <?php } ?>

      <div class="article-img__title">
        <div class="container">
          <h1><?php echo apply_filters( 'the_title', $post->post_title, $post->ID ); ?></h1>
          <div class="article-img__details">
            <p><?php motovise_posted_on(); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="article-content">
      <?php echo apply_filters( 'the_content', $post->post_content, $post->ID ); ?>
  </div>
<?php  motovise_recent_posts( 3 ); ?>
</main>

<?php get_footer(); ?>
