// burger
$('.header-control__burger-wrapper').on('click', function() {
	$('.header-control__burger').toggleClass('header-control__burger--active');
	$('.header-menu').toggleClass('header-menu--active');
	$('.header-control').toggleClass('header-control--open-menu');
	$('.site-main').toggleClass('site-main--blocked');
	$('body').toggleClass('body--colored');
})
 // scroll down
$(function() {
	$('a.scroll-down').bind('click', function(event) {
		var $anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: $($anchor.attr('href')).offset().top
		}, 500);
		event.preventDefault();
	});
});

// carousel index--testimonials
var owl = $(".index-testimonials__slider");
owl.owlCarousel({
	slideSpeed : 300,
	paginationSpeed : 400,
	singleItem: true,
	mouseDrag: true,
	touchDrag: true,
	autoPlay: 5000,
	pagination: false,
});
$(".index-testimonials__slider--right").click(function() {
	owl.trigger('owl.next');
})
$(".index-testimonials__slider--left").click(function() {
	owl.trigger('owl.prev');
})

// how it works video
$(document).ready(function () {
	var close = function() {
		var player = $('.remodal iframe'), video = player.attr("src");
		player.attr("src","");
		player.attr("src", video);
	}
	$(document).on('closing', '.remodal', function (e) {
		close();
	});
});


function hideInfo() {
	$('.about-meet__show-more').addClass('about-meet__show-more--hide');
	$('.meet-item').removeClass('meet-item--active');
}

$('#showMoreBtn1').on('click', function() {
	hideInfo();
	$('#moreItem1').removeClass('about-meet__show-more--hide');
	$(this).parent('.meet-item').addClass('meet-item--active');
})

$('#showMoreBtn2').on('click', function() {
	hideInfo();
	$('#moreItem2').removeClass('about-meet__show-more--hide');
	$(this).parent('.meet-item').addClass('meet-item--active');
})

$('#hideMoreBtn1').on('click', function() {
	hideInfo();
})

$('#hideMoreBtn2').on('click', function() {
	hideInfo();
})