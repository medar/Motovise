jQuery(document).ready(function ($) {
// burger
    $('.header-control__burger-wrapper').on('click', function () {
        $('.header-control__burger').toggleClass('header-control__burger--active');
        $('.header-menu').toggleClass('header-menu--active');
        $('.header-control').toggleClass('header-control--open-menu');
        $('.site-main').toggleClass('site-main--blocked');
        $('body').toggleClass('body--colored');
    });

// scroll down
    $(function () {
        $('a.scroll-down').bind('click', function (event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 500);
            event.preventDefault();
        });
    });

// carousel index--testimonials
    var owl = $(".index-testimonials__slider");
    owl.owlCarousel({
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        mouseDrag: true,
        touchDrag: true,
        autoPlay: 5000,
        pagination: false
    });
    $(".index-testimonials__slider--right").click(function () {
        owl.trigger('owl.next');
    });
    $(".index-testimonials__slider--left").click(function () {
        owl.trigger('owl.prev');
    });

// how it works video
    $(document).ready(function () {
        var close = function () {
            var player = $('.remodal iframe'), video = player.attr("src");
            player.attr("src", "");
            player.attr("src", video);
        };
        $(document).on('closing', '.remodal', function (e) {
            close();
        });
    });

    function get_offset() {
        return $('.blog-card').length;
    }


    $('#infinitescroll-on_click').on('click', function (event) {
        event.preventDefault();
        var data = {
            action: 'infinite_scroll',
            offset: get_offset()
        };
        jQuery.post(ajaxurl, data, function (response) {
            $('.blog-content__wrapper').append(response);
            Waypoint.refreshAll();
        });
    });

    var waypoints = $('#infinitescroll-on_scroll').waypoint(function (direction) {
        var data = {
            action: 'infinite_scroll',
            offset: get_offset()
        };
        jQuery.post(ajaxurl, data, function (response) {
            $('.blog-content__wrapper').append(response);
            Waypoint.refreshAll();
        });

    }, {
        offset: '90%'
    });


});