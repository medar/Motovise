<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	<link href="<?php echo get_template_directory_uri(); ?>/img/favicon-32x32.png" rel="icon" type="image/x-icon">
	<script type="text/javascript">
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	</script>
</head>
<body  <?php body_class(); ?>>
<header class="site-header <?php if (is_front_page()): echo 'site-header--dark'; endif; ?>">
	<div class="header-control">
		<div class="header-control__logo"><a class="home-link" href="/">
				<svg xmlns="http://www.w3.org/2000/svg" width="154" height="28.56" viewbox="0 0 154 28.56">
					<path class="logo-img" fill="#fefefe" d="M17.63 0h3.27a36.35 36.35 0 0 1 8.53 1.2c2.51.65 2.78 3.7 3.72 5.71A31.83 31.83 0 0 1 37 7a1.73 1.73 0 0 1 0 3.16 19.47 19.47 0 0 1-2.09.08c1.57 1 3.69 2.15 3.57 4.3V26.6A1.69 1.69 0 0 1 37 28.49a27 27 0 0 1-5-.1c-1.24-.76-.82-2.37-.92-3.57H6.86c-.1 1.2.31 2.81-.93 3.57a23 23 0 0 1-4.47.1c-.71.09-1.04-.68-1.46-1.11v-13.9a8.87 8.87 0 0 1 3.32-3.28c-.76-.07-1.53-.08-2.3-.13C.69 9.67.35 9.28 0 8.89v-.76C1.18 6.28 3.49 7 5.32 6.92c.62-1.66 1-3.62 2.33-4.83 3-1.63 6.6-1.86 10-2.1m10.3 5.95q-5.71 4.17-11.44 8.32c-2.21-1.38-4.37-2.84-6.6-4.17-.77-.76-2.14.59-1.34 1.34 2.33 3 4.76 5.9 7.18 8.81.48.75 1.34.34 1.76-.23L29.67 7.17a1.07 1.07 0 0 0 .21-1.57c-.65-.6-1.37-.08-1.93.34z"></path>
					<path class="logo-text" fill="#fefefe" d="M125.06 3.57a2 2 0 0 1 2.88 2A1.93 1.93 0 0 1 125 7.05a2.07 2.07 0 0 1 .06-3.48zM88.27 6.05a26.59 26.59 0 0 1 3.57 0v3.36h3.34v2.49h-3.31c0 1.9-.1 3.81.08 5.71.32 1.3 1.88.7 2.74.41.29.85.54 1.71.78 2.58a6.46 6.46 0 0 1-4.88.54A3.36 3.36 0 0 1 88.33 18c-.09-2 0-4.1 0-6.15h-1.75V9.38h1.7c.02-1.1.02-2.21-.01-3.33zm-38.86 2.8h3.51v2.11c1.81-3.09 7.13-2.94 8.36.58 1.38-3.58 7.42-3.83 8.59 0 .63 3.18.15 6.47.3 9.69h-3.55a57.85 57.85 0 0 0-.14-7.74c-.72-2.17-4.15-1.77-4.7.33-.5 2.45-.13 5-.25 7.45h-3.48c-.09-2.57.19-5.16-.16-7.7-.61-2.21-4.06-1.9-4.67.16-.58 2.46-.16 5-.28 7.54h-3.52c-.01-4.18 0-8.27-.01-12.42zm27.12.22a7.58 7.58 0 0 1 6.78 1.19c3 2.49 2.73 7.85-.56 10-3 1.94-7.78 1.39-9.68-1.86s-.47-8.4 3.46-9.33m.65 3.09c-2.43 1.49-1.7 6.12 1.34 6.28 2.28.33 3.81-2.23 3.24-4.25a3 3 0 0 0-4.58-2.03zm23.45-3.07a7.65 7.65 0 0 1 6.73 1c3.17 2.46 2.93 8.09-.54 10.17-2.95 1.77-7.41 1.31-9.4-1.68-2.2-3.25-.79-8.52 3.21-9.53m1 2.9c-2.6 1.27-2.18 6 .82 6.41 2.22.53 4-1.86 3.54-3.93a3.06 3.06 0 0 0-4.35-2.43zm8.14-3.08h3.65c.93 2.92 1.9 5.83 2.81 8.75 1.31-2.78 1.95-5.85 3-8.75h3.52q-2.32 6.19-4.65 12.37h-3.62Q112.1 15 109.77 8.87zm14.5 12.35V8.84h3.52v12.38h-3.53zm8.13-11.84c2.47-1.27 5.47-.65 7.79.66L139 12.45c-1.52-.63-3.87-2-5.2-.47v.54c1.42 1.34 3.74 1.12 5.23 2.41 1.88 1.34 1.47 4.55-.51 5.6a8.26 8.26 0 0 1-8.65-.93q.58-1.16 1.14-2.34c1.61 1 3.65 2 5.56 1.23.8-.64 0-1.62-.75-1.75-1.64-.67-3.67-.83-4.81-2.35a3.54 3.54 0 0 1 1.39-5.01zm12.2.17a7 7 0 0 1 7.47.52c1.69 1.51 2 3.9 1.93 6-3 .05-6-.1-9 .09.88 2.82 4.57 3 6.45 1.12a22.24 22.24 0 0 1 1.83 2 7.73 7.73 0 0 1-9 1.05c-3.73-2.32-3.55-8.73.34-10.81M145 14c1.92.1 3.84.06 5.75.05a2.74 2.74 0 0 0-2.75-2.72c-1.58-.15-2.62 1.26-3 2.67z"></path>
					<?php  
					if ( function_exists( 'ot_get_option' ) ) {
					  $book_inspection_url = ot_get_option( 'book_inspection_url', '#' );
					}
					?>
				</svg></a><a class="inspection-link" href="<?php echo $book_inspection_url; ?>"><span></span>Book Inspection</a></div>
		<div class="header-control__burger-wrapper"><span>Menu</span>
			<div class="header-control__burger"><span></span></div>
		</div>
	</div>
	<div class="header-menu">
		<div class="header-menu__bg"></div>
		<div class="header-menu__content">
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'menu-1',
						'menu_id'         => 'primary-menu',
						'container'       => 'div',
						'container_class' => 'header-menu__items',
						)
					);
					
					
					if ( function_exists( 'ot_get_option' ) ) { 
						$site_phone_2 = ot_get_option( 'site_phone_2', '#' );
					}
				?>
			<div class="header-menu__social"><a href="tel:+<?php echo str_replace('-', '', $site_phone_2 ); ?>">Call Us: <?php echo $site_phone_2; ?></a></div>
		</div>
	</div>
</header>
