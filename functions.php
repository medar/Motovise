<?php
/**
 * motovise functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

if ( ! function_exists( 'motovise_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function motovise_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/*
		 * This theme uses wp_nav_menu() in one location.
		 */
		register_nav_menus( array(
			'menu-1' => esc_html( 'Primary' ),
			'menu-2' => esc_html( 'Footer menu' )
		) );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'post-preview', 360, 220, true );
		add_image_size( 'post-header-full', 1440, 600 );
		add_image_size( 'post-header-medium', 768, 600 );
		add_image_size( 'post-header-small', 480, 480 );
		add_image_size( 'post-header-xsmall', 360, 480 );


	}
endif;
add_action( 'after_setup_theme', 'motovise_setup' );

if ( ! function_exists( 'motovise_scripts' ) ) :
	function motovise_scripts() {

		wp_enqueue_style(
			'motovise-libs',
			get_template_directory_uri() . '/css/libs.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'motovise-styles',
			get_template_directory_uri() . '/css/styles.css',
			array( 'motovise-libs' ),
			'2.2.0',
			'all'
		);

		wp_enqueue_style(
			'matchmaker-fonts',
			'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Roboto+Slab:700|Roboto:400,500,700',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'motovise-style',
			get_stylesheet_uri(),
			array( 'motovise-libs', 'motovise-styles' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_script(
			'waypoints-js',
			'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js',
			array(),
			'4.0.1',
			true
		);

		wp_enqueue_script(
			'motovise-bootstrap',
			'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',
			array( 'jquery' ),
			'3.3.6',
			true
		);

		wp_enqueue_script(
			'motovise-libs',
			get_template_directory_uri() . '/js/libs.js',
			array( 'motovise-bootstrap' ),
			'1.0.0',
			true
		);

		wp_enqueue_script(
			'motovise-js',
			get_template_directory_uri() . '/js/scripts.js',
			array( 'motovise-libs', 'waypoints-js' ),
			'1.0.0',
			true
		);

	}
endif;

add_action( 'wp_enqueue_scripts', 'motovise_scripts' );

function motovise_get_posts( $posts_count, $posts_offset = 0 ) {
	$args         = array(
		'numberposts' => $posts_count,
		'post_status' => 'publish',
		'offset'      => $posts_offset
	);
	$recent_posts = wp_get_recent_posts( $args );
	foreach ( $recent_posts as $recent ) {
		$post_image       = false;
		$excerpt_length   = 50;
		$blog_card_no_img = " blog-card__no-img";
		if ( has_post_thumbnail( $recent["ID"] ) ) :
			$post_image       = true;
			$excerpt_length   = 15;
			$blog_card_no_img = '';
		endif;

		$content = $recent["post_content"];
		$excerpt = wp_trim_words( $content, $num_words = $excerpt_length, $more = null );
		$time    = date( 'm.d.Y', strtotime( $recent["post_date"] ) );
		echo '<div class="blog-card">';
		if ( $post_image ) :
			echo '<a class="blog-card__img" href="'
			     . get_permalink( $recent["ID"] ) . '">';
			echo get_the_post_thumbnail( $recent["ID"], $size = 'post-preview' );
			echo '</a>';
		endif;

		echo '<div class="blog-card__text' . $blog_card_no_img . '">'
		     . '<a href="' . get_permalink( $recent["ID"] ) . '"><h3>'
		     . $recent["post_title"]
		     . '</h3></a>'
		     . '<p>' . $excerpt . '</p>'
		     . '</div>'

		     . '<div class="blog-card__date' . $blog_card_no_img . '">'
		     . '<p>' . $time . '</p>'
		     . '</div>'

		     . '</div>';
	}
	wp_reset_query();
}

function motovise_recent_posts( $posts_count = 3, $infinite_scroll = false ) {
	$output = '';
	ob_start();
	?>
	<div class="blog-content <?php if ( is_single() ): echo 'blog-content--relevant'; endif; ?>">
		<div class="container">
			<div class="blog-content__wrapper">

				<?php motovise_get_posts( $posts_count ); ?>

			</div>
			<?php if ($infinite_scroll):?>
			<a href="#" id="infinitescroll-on_scroll">Load more</a>
		<?php endif; ?>
		</div>
	</div>
	<?php
	wp_reset_postdata();
	$output .= ob_get_clean();

	echo $output;
}

function motovise_search_result() {
	$output = '';
	ob_start();
	?>
	<div class="blog-content <?php if ( is_single() ): echo 'blog-content--relevant'; endif; ?>">
		<div class="container">
			<div class="blog-content__wrapper">
				<?php
				global $post;
				while ( have_posts() ) : the_post();
					$post_image       = false;
					$excerpt_lenght   = 50;
					$blog_card_no_img = " blog-card__no-img";
					if ( has_post_thumbnail( $post->ID ) ) :
						$post_image       = true;
						$excerpt_lenght   = 15;
						$blog_card_no_img = '';
					endif;

					$content = $post->post_content;
					$excerpt = wp_trim_words( $content, $num_words = $excerpt_lenght, $more = null );
					$time    = date( 'm.d.Y', strtotime( $post->post_date ) );
					echo '<div class="blog-card">';
					if ( $post_image ) :
						echo '<a class="blog-card__img" href="'
						     . get_permalink( $post->ID ) . '">';
						echo get_the_post_thumbnail( $post->ID, $size = 'post-preview' );

						echo '</a>';
					endif;

					echo '<div class="blog-card__text' . $blog_card_no_img . '">'
					     . '<a href="' . get_permalink( $post->ID ) . '"><h3>'
					     . $post->post_title
					     . '</h3></a>'
					     . '<p>' . $excerpt . '</p>'
					     . '</div>'

					     . '<div class="blog-card__date' . $blog_card_no_img . '">'
					     . '<p>' . $time . '</p>'
					     . '</div>'

					     . '</div>';

				endwhile;
				?>
			</div>
		</div>
	</div>
	<?php
	wp_reset_postdata();
	$output .= ob_get_clean();

	echo $output;
}

function motovise_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'matchmaker' ),
		$time_string
	);

	echo '<span class="posted-on">' . $posted_on . '</span>';
}

function motovise_register_types() {
	register_post_type( 'motovise_testimonial', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Testimonials',
			'singular_name'      => 'Testimonial',
			'add_new'            => 'Add testimonials',
			'add_new_item'       => 'Add testimonial',
			'edit_item'          => 'Edit testimonial',
			'new_item'           => 'New testimonial',
			'view_item'          => 'View testimonial',
			'search_items'       => 'Search testimonial',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Testimonials',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-slides',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
	
	register_post_type( 'motovise_team', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Team',
			'singular_name'      => 'Team',
			'add_new'            => 'Add team',
			'add_new_item'       => 'Add team',
			'edit_item'          => 'Edit team',
			'new_item'           => 'New team',
			'view_item'          => 'View team',
			'search_items'       => 'Search team',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Team',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-universal-access',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );

	register_post_type( 'motovise_faq', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'FAQs',
			'singular_name'      => 'FAQ',
			'add_new'            => 'Add FAQs',
			'add_new_item'       => 'Add FAQ item',
			'edit_item'          => 'Edit FAQ item',
			'new_item'           => 'New FAQ item',
			'view_item'          => 'View FAQ iteml',
			'search_items'       => 'Search FAQ item',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'FAQs',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-admin-comments',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );

	register_post_type( 'motovise_inspections', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Inspections',
			'singular_name'      => 'Inspections',
			'add_new'            => 'Add Inspections',
			'add_new_item'       => 'Add Inspection item',
			'edit_item'          => 'Edit Inspection item',
			'new_item'           => 'New Inspection item',
			'view_item'          => 'View Inspection item',
			'search_items'       => 'Search Inspection item',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Inspections',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-yes',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
}
add_action( 'init', 'motovise_register_types' );

function motovise_remove_autop( $content ) {
	global $post;

	if ( $post->post_type == 'motovise_testimonial' ):
		remove_filter( 'the_content', 'wpautop' );
	endif;

	if ( $post->post_type == 'motovise_faq' ):
		remove_filter( 'the_content', 'wpautop' );
	endif;

	if ( $post->post_type == 'motovise_inspections' ):
		remove_filter( 'the_content', 'wpautop' );
	endif;

	return $content;
}
add_filter( 'the_content', 'motovise_remove_autop', 0 );

function motovise_search_form( $form ) {

	$form = '
	<form class="search-form" role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
		<input class="search-field" type="search" value="' . get_search_query() . '" name="s" id="s" placeholder="Search …" title="Search for:" />
		<input class="search-submit" type="submit" id="searchsubmit" value="" />
	</form>';


	return $form;
}
add_filter( 'get_search_form', 'motovise_search_form' );

function ajax_motovise_infinite_scroll_callback() {

	$offset = intval( $_POST['offset'] );

	motovise_get_posts( $posts_count = 3, $posts_offset = $offset );

	wp_die();
}
add_action( 'wp_ajax_infinite_scroll', 'ajax_motovise_infinite_scroll_callback' );
add_action( 'wp_ajax_nopriv_infinite_scroll', 'ajax_motovise_infinite_scroll_callback' );

remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );

function disable_wp_emojicons() {
  // Все экшены, связанные с emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  // Фильтр для удаления emojis TinyMCE
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );
function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}
add_filter( 'emoji_svg_url', '__return_false' );

// only post in search
function prefix_limit_post_types_in_search( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', array( 'post' ) );
    }
    return $query;
}
add_filter( 'pre_get_posts', 'prefix_limit_post_types_in_search' );

// hide preview button and slug area on custom type
function posttype_admin_css() {
global $post_type;
$post_types = array(
	/* set post types */
	'post',
	'page',
);
if(!in_array($post_type, $post_types))
echo '<style type="text/css">#post-preview, #view-post-btn, #edit-slug-box{display: none;}</style>';
}
add_action( 'admin_head-post-new.php', 'posttype_admin_css' );
add_action( 'admin_head-post.php', 'posttype_admin_css' );